class Poller {
  // Straightforward constructor
  constructor(fn, frequency) {
    this.fn = fn;
    this.frequency = frequency;
  }

  // This is where the magic happens
  [Symbol.iterator]() {
    return {
      next: () => {
        const value = new Promise((resolve, reject) => {
          // We could expect a setInterval here,
          // but the iterator and the loop make it useless
          // And we don't have to manage its cleaning
          setTimeout(() => {
            try {
              resolve(this.fn());
            } catch (err) {
              reject(err);
            }
          }, this.frequency);
        });
        // This craziness has no limit!
        return { value, done: false };
      },
    };
  }
}

module.exports = Poller;
