"use strict";

const assert = require("assert");
const sinon = require("sinon");
const FakeTimers = require("@sinonjs/fake-timers");

const Poller = require("./poller");

it("creates an iterator", function () {
  const poller = new Poller(sinon.stub(), 1000);
  assert.strictEqual(typeof poller[Symbol.iterator], "function");
});

[
  {
    fnType: "a sync function",
    stub: sinon.stub().returns("response"),
  },
  {
    fnType: "an async function",
    stub: sinon.stub().resolves("response"),
  },
].forEach((ctx) => {
  it(`iterator waits for executing ${ctx.fnType}`, async function () {
    const clock = FakeTimers.install();
    const delay = 1000;
    const poller = new Poller(ctx.stub, delay);
    const iterator = poller[Symbol.iterator]();

    try {
      const { value } = iterator.next();
      clock.tick(delay - 1);
      sinon.assert.notCalled(ctx.stub);
      clock.tick(1);
      sinon.assert.calledOnce(ctx.stub);
      const res = await value;
      assert.strictEqual(res, "response");
    } finally {
      clock.uninstall();
    }
  });
});

[
  {
    fnType: "a sync function",
    stub: sinon.stub().throws("error in function"),
  },
  {
    fnType: "an async function",
    stub: sinon.stub().rejects("error in function"),
  },
].forEach((ctx) => {
  it(`iterator returns a rejected promise in case of error in ${ctx.fnType}`, async function () {
    const clock = FakeTimers.install();
    const delay = 1000;
    const poller = new Poller(ctx.stub, delay);
    const iterator = poller[Symbol.iterator]();

    try {
      const { value } = iterator.next();
      clock.tick(delay);
      await assert.rejects(value, /error in function/);
    } finally {
      clock.uninstall();
    }
  });
});
