const Poller = require("./poller");

let counter = 1;

async function sendPing() {
  // Fancy error simulation
  if (counter % 5 === 0) {
    counter++;
    throw new Error("oh no!");
  }
  return `ping => ${counter++}`;
}

async function main() {
  // Our poller is just an object
  const poller = new Poller(sendPing, 1000);

  // We can force the execution by simply calling the function
  // Just do not forget to handle the errors
  try {
    const res = await sendPing();
    console.log("Forced execution: ", res);
  } catch (err) {
    console.error(`Error while forcing execution: "${err.message}"`);
  }

  // for..of loop allows simple handling of the poller
  for (const resP of poller) {
    try {
      const res = await resP;
      console.log(res);
    } catch (err) {
      console.error(`Handled error: "${err.message}"`);
    }
  }
}

main();
